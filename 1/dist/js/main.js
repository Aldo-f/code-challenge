/**
 * Global vars
 */
var
    groupList = [],
    idLastGroup = 0;
const
    DEBUG = true,
    structure = {
        id: 1,
        name: "Parent",
        children: [{
                id: 2,
                name: "Child",
                children: [{
                    id: 4,
                    name: "Grandchild",
                    children: []
                }, {
                    id: 5,
                    name: "Grandchild",
                    children: []
                }]
            },
            {
                id: 3,
                name: "Child",
                children: [{
                        id: 6,
                        name: "Grandchild",
                        children: []
                    },
                    {
                        id: 7,
                        name: "Grandchild",
                        children: []
                    }
                ]
            }
        ]
    };

/**
 * Start application
 */
(function () {
    debugger;
    //Get nodes from structure
    let node1 = findNodeById(structure, 1);
    let node2 = findNodeById(structure, 2);
    let node3 = findNodeById(structure, 3);
    let node7 = findNodeById(structure, 7);

    if (DEBUG) {
        console.log('Show node 7');
        console.log(node7);
        console.log('____');
    }

    // Create groups
    createGroup(node1, node2, node3, node3, node3)
    createGroup(node1, node2)
    createGroup(node2, node3, node1)
    createGroup(node1, node2)
    createGroup(findNodeById(structure, 5));

    if (DEBUG) {
        console.log(`The groupList has ${groupList.length} groups`);
        console.log(groupList);
        console.log('____');
    }

    // Remove group from groupList
    removeGroup(3);
    removeGroup(50); // group does not exist

    if (DEBUG) {
        console.log(
            `The groupList has ${groupList.length} groups.\nGroup with id 3 has been deleted`);
        console.log(groupList);
        console.log('____');
    }

    // Add node to group
    addNodeToGroup(node3, 4); // add node3 to group with id 4
    addNodeToGroup(findNodeById(structure, 5), 50); // group does not exist
    addNodeToGroup(findNodeById(structure, 6), idLastGroup); // add node to last created group

    if (DEBUG) {
        console.log('node3 has been added to group with id 4');
        console.log(groupList[returnIndex(4)]);
        console.log('node 6 has been added to the last group');
        console.log(groupList[returnIndex(idLastGroup)]);
        console.log('____');
    }

    // Remove node from group
    removeNodeFromGroup(node1, 1);
    removeNodeFromGroup(node7, 5); // group 5 does not have node7
    removeNodeFromGroup(node3, 1); // group 1 has multiple node3

    // Find all groups containing node
    let groups1 = findGroupsWithNode(node3); // node 3 is two times in group with id 1
    let groups2 = findGroupsWithNode(node1);

    if (DEBUG) {
        console.log(groups1);
        console.log(groups2);
    }
})();

/**
 * Find node by the id of the node  
 * @param {*} obj is the structure to search in
 * @param {*} nodeId is the id of the node
 */
function findNodeById(obj, nodeId) {
    if (obj.id === nodeId) {
        return obj
    }
    if (obj.children) {
        for (let item of obj.children) {
            let check = findNodeById(item, nodeId)
            if (check) {
                return check
            }
        }
    }
    return null
}

/**
 * Creates a group with id 
 * @param  {...any} nodes 
 */
function createGroup(...nodes) {
    idLastGroup++;
    let group = {
        id: idLastGroup,
        nodes: nodes
    };
    groupList.push(group);
}

/**
 * Return the index of the group inside the groupList
 * @param {*} groupId 
 */
function returnIndex(groupId) {
    let i = groupList.map(function (x) {
        return x.id
    }).indexOf(groupId);
    return i;
}

/**
 * Removes a group from groupList
 * @param {*} groupId 
 */
function removeGroup(groupId) {
    let i = returnIndex(groupId);

    if (i > -1) groupList.splice(i, 1);
}

/**
 * Adds a node to a group 
 * @param {*} node 
 * @param {*} groupId must exist
 */
function addNodeToGroup(node, groupId) {
    let i = returnIndex(groupId);

    if (i > -1) groupList[i].nodes.push(node);
}

/**
 * Removes a node from specified group
 * @param {*} node 
 * @param {*} groupId 
 */
function removeNodeFromGroup(node, groupId) {
    let indexGroup = returnIndex(groupId);
    let indexNode = groupList[indexGroup]
        .nodes.map(function (x) {
            return x.id
        }).indexOf(node.id);

    if (indexNode > -1) groupList[indexGroup]
        .nodes.splice(indexNode, 1);
}

/**
 * Find all groups containing node
 * @param {*} node 
 */
function findGroupsWithNode(node) {
    let groupsWithNode = [];

    groupList.forEach(group => {
        for (const n of group.nodes) {
            if (n.id === node.id) {
                groupsWithNode.push(group);
                return;
            }
        }
    });
    return groupsWithNode;
}