# Ember.js

## Tutorial
``` sh
npm install -g ember-cli
ember new ember-quickstart
cd ember-quickstart
npm start
ember generate route scientists
ember generate component people-list
ember build --env production

ember help [generate]
```

## Calculator

``` sh 
ember new calculator && cd calculator && npm start
ember install ember-bootstrap
ember install ember-cli-mirage
ember g adapter application
ember g ember-cli-sass


# one line 
ember new calculator && cd calculator && ember install ember-bootstrap
```

### Functions
* [x] input field that accepts ~~a digit~~ number &rarr; should be able to add a number instead of a digit (screenshot)
  * [x] Above input field the sum is displayed 
* [x] sum is bold and has a green background if sum is even and not zero
* [x] reset btn
* [x] remove btn for each number of the list
  * [x] btn removes number 
  * [x] adjusts sum
* [x] display message ("Heads up!) if sum > 100