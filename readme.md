# Code challenge 

## 1. JavaScript (CRUD, taxonomy)
Consider the following nested structure of nodes (a node is an object with properties id, name, children):

var structure = { id: 1, name: "Parent", children: [{id: 2, name: "Child", children: [...]}, {id: 3, name: "Child", children: [...]}] }

Below a visualization of the nested structure.

Now we want to create a number of methods that, starting from the above object, allow us to create groups of nodes. For example, a group containing the grandparent and the 4 grandchildren.

Question:
Develop the minimum required methods:

* to create a new group with specified nodes, and add this group to a list of groups
* to find within the list of groups: "find all groups containing node X"
* to update a group: "add node to an existing group", "remove node from an existing group"
* to remove a group from the list of groups


![challenge 1](_bin/1.png)

## 2. Ember.js calculator
The 2nd part aims to let you taste Ember (http://emberjs.com/), and is in principle not very difficult. On the Ember website there are (video) guides that should get you up to speed real quick.

Question:
Create a simple calculator that runs in the browser, with the following functionalities:

1. There is an input field in which you enter a digit, and an 'Add' button, which adds the digit to a list of previously entered figures. Above the input field, the sum of all entered numbers is displayed.
2. The sum is shown in bold and with a green background if it is even and not zero.

3. There is a reset button. The sum is reset to 0, and the list of entered numbers is cleared.

4. There is a remove button for each number in the list. This button deletes the number from the list and adjusts the sum.

5. A text message (“Heads up!) is displayed if the sum is greater than 100.

I have made a small sketch below:

![challenge 2](_bin/2.png)



------ 

Clone with submodules
``` sh 
git clone --recurse-submodules https://gitlab.com/Aldo-f/code-challenge.git 
```